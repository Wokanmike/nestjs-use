/**
 * This is a database module, is not an internal concept of Nestjs but this module have some special characteristics
 * 1. This module is created the same way as the other modules.
 * 2. This module is used to create the connection with the database, and also to create the entities.
 * 3. Uses a decorator called @Global ,above the @Module decorator, this is a decorator that is used to make the module exportable whitout the need of import it in the other modules.
 * 4. You can see on the code 2 different ways to create the connection with the database
 *  a. With the 'pg' Client
 *  b. With the 'typeOrmModule' Client
 * 5. Both are factories and the are exported to all the modules using the exports.
 */

import { Module, Global } from '@nestjs/common';
import { ConfigType } from '@nestjs/config';
import { Client } from 'pg';
import { TypeOrmModule } from '@nestjs/typeorm';

import config from './../config';

const API_KEY_PROD = 'PROD12345';
const API_KEY_2 = '23443252435';

@Global()
@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      inject: [config.KEY],
      useFactory: async (configService: ConfigType<typeof config>) => {
        const { name, user, host, port, password } = configService.database;
        return {
          type: 'postgres',
          host,
          port,
          username: user,
          password,
          database: name,
          synchronize: false, //on dev mode, drop and create the database
          autoLoadEntities: true,
        };
      },
    }),
  ],
  providers: [
    {
      provide: 'API_KEY',
      useValue:
        process.env.NODE_ENV === 'production' ? API_KEY_PROD : API_KEY_2,
    },
    {
      provide: 'PG',
      useFactory: async (configService: ConfigType<typeof config>) => {
        const { name, user, host, port, password } = configService.database;
        const client = new Client({
          user,
          host,
          database: name,
          password,
          port,
        });
        client.connect();
        return client;
      },
      inject: [config.KEY],
    },
  ],
  exports: ['API_KEY', 'PG', TypeOrmModule],
})
export class DatabaseModule {}
