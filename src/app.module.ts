/** This is the main module, important considerations:
 * 1. The module is the main file of the application, it is the entry point of the application.
 * 2. Here you must import the modules that you want to use in the application. ej: import { UsersModule } from './users/users.module';
 * 3. Here you can make the validations of the environment variables, if you want to use the environment variables in the application.
 * 4. Notice that the module have 3 blocks, one is the imports, one is the controllers, one is the providers.
 * 5. The imports block is where you must import the modules that you want to use in the application.
 * 6. The controllers block is where you must import the controllers that you want to use in the application like the root of the routes.
 * 7. The providers block is where you must import the providers that you want to use in the application and they could be called on other service using the decorator @Inject.
 * 8. Inside the providers you can have:
 *  - useFactory: to create a factory, where you can have dependency injections and async processes.
 *  - useValue: to create a value, also Injectable and exportable.
 */

import { Module } from '@nestjs/common';
import { HttpModule, HttpService } from '@nestjs/axios';
import { ConfigModule } from '@nestjs/config';
import * as Joi from 'joi';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersModule } from './users/users.module';
import { ProductsModule } from './products/products.module';
import { lastValueFrom } from 'rxjs';
import { DatabaseModule } from './database/database.module';

import { environments } from './environments';
import { AuthModule } from './auth/auth.module';
import config from './config';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: environments[process.env.NODE_ENV] || '.env',
      load: [config],
      isGlobal: true,
      validationSchema: Joi.object({
        API_KEY: Joi.string().required(),
        DB_NAME: Joi.string().required(),
        DB_PORT: Joi.number().required(),
        DB_HOST: Joi.string().required(),
        DB_USER: Joi.string().required(),
        DB_PASSWORD: Joi.number().required(),
      }),
    }),
    HttpModule,
    UsersModule,
    ProductsModule,
    DatabaseModule,
    AuthModule,
  ],
  controllers: [AppController],
  providers: [
    AppService,
    {
      //UseFactory have the characteristics of been async and have dependency injection
      provide: 'TASKS',
      useFactory: async (http: HttpService) => {
        const tasks = await lastValueFrom(
          http.get('https://jsonplaceholder.typicode.com/todos'),
        );
        return tasks.data;
      },
      inject: [HttpService],
    },
  ],
})
export class AppModule {}
