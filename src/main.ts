/**
 * This is the main file of the application, this file is the entry point of the application
 * Here you can enable some features like swagger or cors
 * For example here we are enabling the ValidationPipe to validate the data that we are getting from the request
 * and setting the kind of responses for some cases
 * Also we are enabling the SwaggerModule to create the documentation of the API
 */

import { NestFactory, Reflector } from '@nestjs/core';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { ValidationPipe, ClassSerializerInterceptor } from '@nestjs/common';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true, // this only takes the correct params stablished in the DTO
      forbidNonWhitelisted: true, //send aa bad request if the data is not valid
      transformOptions: {
        enableImplicitConversion: true, //this transforms the data to the type that the model expects
      },
    }),
  );

  app.useGlobalInterceptors(new ClassSerializerInterceptor(app.get(Reflector)));

  const config = new DocumentBuilder()
    .setTitle('API Demo Documentation')
    .setDescription('Tuto of NestJS')
    .setVersion('1.0')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('docs', app, document);
  //this configuration can enable the api to global config or to an specific domain
  // app.enableCors();

  await app.listen(3000);
}
bootstrap();
