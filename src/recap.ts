const suma = (a: number, b: number) => {
  return a + b;
};
suma(34, 12);

class Persona {
  /**
   *
   */
  constructor(private age: number, private name: string) {}
  getSummary() {
    return `${this.name} is ${this.age} years old.`;
  }
}

const nicolas = new Persona(30, 'Nicolas');
nicolas.getSummary();
