/* This file help to search the different .env files to run on different environments */
export const environments = {
  dev: '.env',
  stag: '.stag.env',
  prod: '.prod.env',
};
