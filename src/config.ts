/**This config file helps to type the environment variables, on the object they can be clasified,
 * this file also is imported on the app.module file where the validations are made*/

import { registerAs } from '@nestjs/config';

export default registerAs('config', () => {
  return {
    database: {
      name: process.env.DB_NAME,
      apiKey: process.env.API_KEY,
      port: parseInt(process.env.DB_PORT, 10),
      host: process.env.DB_HOST,
      user: process.env.DB_USER,
      password: process.env.DB_PASSWORD,
      synchronize: process.env.TYPEORM_SYNCHRONIZE,
      logging: process.env.TYPEORM_LOGGING,
      entities: process.env.TYPEORM_ENTITIES,
      migrations: process.env.TYPEORM_MIGRATIONS,
    },
    apiKey: process.env.API_KEY,
  };
});
