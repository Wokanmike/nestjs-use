/** On this service you can learn the next things:
 * 1. Because this service is connected to a database, you can use the repository to interact with the database, with 'import { Repository } from 'typeorm';'
 * 2. You can use the 'import { InjectRepository } from '@nestjs/typeorm';' to inject the repository in the constructor
 * 3. Learn some common queries to use in the repository like 'find', 'findOneBy', 'save', 'update', 'softDelete'
 * 4. As you can see on the method create and update you have a different way to create and update an object, this is because
 *   the repository has a method called 'merge' that merge the changes of the object, and one called 'create' that create a new object
 *   mapping the data of the dto, and one called 'save' that save the object in the database.
 */

import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { In, Repository, Between, FindOptionsWhere } from 'typeorm';

import { Product } from '../entities/product.entity';
import { Category } from '../entities/category.entity';
import {
  CreateProductDto,
  UpdateProductDto,
  FilterProductsDto,
} from '../dtos/products.dtos';
import { Brand } from '../entities/brand.entity';

import { BrandsService } from './brands.service';

@Injectable()
export class ProductService {
  /**
   *
   */
  constructor(
    @InjectRepository(Product) private productRepo: Repository<Product>,
    @InjectRepository(Category) private categoryRepo: Repository<Category>,
    @InjectRepository(Brand) private brandsRepo: Repository<Brand>,
    private brandsService: BrandsService,
  ) {}

  findAll(params?: FilterProductsDto) {
    if (params) {
      const where: FindOptionsWhere<Product> = {};
      const { limit, offset } = params;
      const { maxPrice, minPrice } = params;
      if (minPrice && maxPrice) {
        where.price = Between(minPrice, maxPrice);
      }
      return this.productRepo.find({
        where,
        relations: ['brand', 'categories'],
        skip: offset,
        take: limit,
      });
    }
    return this.productRepo.find({
      relations: ['brand', 'categories'],
    });
  }

  async findOne(id: number) {
    const product = await this.productRepo.findOne({
      where: { id },
      relations: ['brand', 'categories'],
    });
    if (!product) {
      throw new NotFoundException(`Product with id ${id} not found`);
    }
    return product;
  }

  async create(payload: CreateProductDto) {
    // const newProduct = new Product();
    // newProduct.name = payload.name;
    // newProduct.price = payload.price;
    // newProduct.description = payload.description;
    // newProduct.image = payload.image;
    // newProduct.stock = payload.stock;
    const newProduct = this.productRepo.create(payload);
    if (payload.brandId) {
      const brand = await this.brandsRepo.findOneBy({ id: payload.brandId }); //findOneBy({ id })
      newProduct.brand = brand;
    }
    if (payload.categoriesIds) {
      const categories = await this.categoryRepo.findBy({
        id: In(payload.categoriesIds),
      });
      newProduct.categories = categories;
    }
    return this.productRepo.save(newProduct);
  }

  async update(id: number, payload: UpdateProductDto) {
    const product = await this.findOne(id);
    if (payload.brandId) {
      const brand = await this.brandsRepo.findOneBy({ id: payload.brandId });
      product.brand = brand;
    }
    if (payload.categoriesIds) {
      const categories = await this.categoryRepo.findBy({
        id: In(payload.categoriesIds),
      });
      product.categories = categories;
    }
    this.productRepo.merge(product, payload);
    return this.productRepo.save(product);
  }

  delete(id: number) {
    return this.productRepo.softDelete(id);
  }

  async removeCategoryByProduct(productId: number, categoryId: number) {
    const product = await this.findOne(productId);
    const category = await this.categoryRepo.findOneBy({ id: categoryId });
    product.categories = product.categories.filter(
      (categoryItem) => categoryItem.id !== category.id,
    );
    return this.productRepo.save(product);
  }

  async addCategoryToProduct(productId: number, categoryId: number) {
    const product = await this.findOne(productId);
    const category = await this.categoryRepo.findOneBy({ id: categoryId });
    product.categories.push(category);
    return this.productRepo.save(product);
  }
}
