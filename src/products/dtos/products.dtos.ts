/**
 * This is a DTO (Data Transfer Object), used to transfer data between the services and the controllers.
 * Here we are using 'class-validator' to validate the data that we are getting from the request
 *   - With different decorators we can define the type of the data that we are getting from the request ex: @IsString(), @IsNumber(), @IsBoolean(), etc.
 *   - Its important to know that this calidation will help to response with a bad request if the data is not valid and handle that error
 * And also we are using swagger to create the documentation of the API.
 *   - with the decorator @ApiProperty() we can define the name of the property, and the type of the property
 * The last line of the DTO uses partialType to extend the class, and make the properties optional.
 */

import {
  IsString,
  IsNumber,
  IsUrl,
  IsNotEmpty,
  IsPositive,
  IsArray,
  IsOptional,
  Min,
  ValidateIf,
} from 'class-validator';

import { PartialType, ApiProperty } from '@nestjs/swagger';

export class CreateProductDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ description: 'Name of the product' })
  readonly name: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  readonly description: string;

  @IsNumber()
  @IsNotEmpty()
  @IsPositive()
  @ApiProperty()
  readonly price: number;

  @IsNumber()
  @IsNotEmpty()
  @IsPositive()
  @ApiProperty()
  readonly stock: number;

  @IsUrl()
  @IsNotEmpty()
  @ApiProperty()
  readonly image: string;

  @IsNotEmpty()
  @IsPositive()
  @ApiProperty()
  readonly brandId: number;

  @IsNotEmpty()
  @IsArray()
  @ApiProperty()
  readonly categoriesIds: number[];
}

export class UpdateProductDto extends PartialType(CreateProductDto) {}

// To build paginations

export class FilterProductsDto {
  @IsOptional()
  @IsPositive()
  limit: number;

  @IsOptional()
  @Min(0)
  offset: number;

  @IsOptional()
  @IsPositive()
  minPrice: number;

  @ValidateIf((item) => item.minPrice)
  @IsPositive()
  maxPrice: number;
}
