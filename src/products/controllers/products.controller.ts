/**
 * Beside the basic CRUD operations, we also need to add a few more operations seen on app.controller.ts,
 * We have the next annotations:
 * 1. We have different decorators to handle the data inputs on the requests, like @Body(), @Query(), @Param(), @Header(), @Cookie(), etc.
 * 2. We can have pipes, this is useful to transform the data, like the pipe 'transform' that transform the data to uppercase or string to number.
 *    Ex: @Param('id', PaserIntPipe)
 * 3. You can use the pipes that @nestjs/common has, like the pipe 'trim' that remove the spaces from the string or you can create your own pipe.
 */

import {
  Controller,
  Get,
  Param,
  Post,
  Query,
  Body,
  Put,
  Delete,
  HttpStatus,
  HttpCode,
  // ParseIntPipe,
} from '@nestjs/common';
import { ApiTags, ApiOperation } from '@nestjs/swagger';

import { ProductService } from '../services/product.service';
import { PaserIntPipe } from '../../common/paser-int.pipe';
import {
  CreateProductDto,
  UpdateProductDto,
  FilterProductsDto,
} from '../dtos/products.dtos';

@ApiTags('Products')
@Controller('products')
export class ProductsController {
  constructor(private productService: ProductService) {}

  @Get('filter')
  getFilter() {
    return `Im a filter`;
  }

  @Get(':id')
  @HttpCode(HttpStatus.ACCEPTED)
  getProducts(@Param('id', PaserIntPipe) id: number) {
    return this.productService.findOne(id);
  }

  @Get()
  @ApiOperation({ summary: 'List of all the products' })
  getByQuery(@Query() params: FilterProductsDto) {
    //pagination ober query
    return this.productService.findAll(params);
  }

  @Post()
  create(@Body() payload: CreateProductDto) {
    return this.productService.create(payload);
  }

  @Put(':id')
  update(
    @Param('id', PaserIntPipe) id: number,
    @Body() payload: UpdateProductDto,
  ) {
    return this.productService.update(id, payload);
  }

  @Put(':id/category/:categoryId')
  addCategory(
    @Param('id', PaserIntPipe) id: number,
    @Param('categoryId', PaserIntPipe) categoryId: number,
  ) {
    return this.productService.addCategoryToProduct(id, categoryId);
  }

  @Delete(':id')
  delete(@Param('id', PaserIntPipe) id: number) {
    return this.productService.delete(id);
  }

  @Delete(':id/category/:categoryId')
  deleteCategory(
    @Param('id', PaserIntPipe) id: number,
    @Param('categoryId', PaserIntPipe) categoryId: number,
  ) {
    return this.productService.removeCategoryByProduct(id, categoryId);
  }
}
