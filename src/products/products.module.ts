/** This a module, the way to create a module with the CLI of NestJS, is with the command:
 * nest g mo <name>, this is useful beacuse edit the app.module to make the import of this module
 * also create all what a module needs, like the controllers, services, etc.
 * Also the other important thing is that on the imports you need to import the TypeOrmModule.forFeature
 * This is because this module is a module that is related to the database, and this module is used to
 * create the connection with the database, and also to create the entities.
 * inside the array of forFeature you need to pass the array of entities that you want to create
 * The other component of the module is the exports, this is the way to export the module, so you can
 * import the services on the other modules.
 */

import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { ProductsController } from './controllers/products.controller';
import { ProductService } from './services/product.service';
import { Product } from './entities/product.entity';
import { BrandsController } from './controllers/brands.controller';
import { BrandsService } from './services/brands.service';
import { Brand } from './entities/brand.entity';
import { CategoriesController } from './controllers/categories.controller';
import { CategoriesService } from './services/categories.service';
import { Category } from './entities/category.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Product, Brand, Category])],
  controllers: [ProductsController, CategoriesController, BrandsController],
  providers: [ProductService, BrandsService, CategoriesService],
  exports: [ProductService, TypeOrmModule],
})
export class ProductsModule {}
