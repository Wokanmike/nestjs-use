/**This is an entity, used to create the database, and to create the tables
 * Here we are using typeorom to manage each column of the table, and also to create the relations
 * 1. You need to use the decorator @Entity() to create the entity
 * 2. The entity name is the name of the table, and the name of the columns is the name of the properties
 * 3. The decorator @PrimaryColumn() is used to define the primary key of the table, and the type of the column
 * 4. The decorator @Column() is used to define the type of the column, and also the options of the column
 * 5. You also can hava the definition of the kind of data that the column can have, like the length of the column,
 *    the precision of the column, the scale of the column, the nullable of the column, the unique of the column,
 *    the default value of the column, the comment of the column, and the onUpdate of the column
 * 6. Its important to check the relations between the tables, on this case a lot of products can have one brand thats why we use @ManyToOne
 * 7. When you use the @ManyToOne, you DONT need to use the decorator @JoinColumn().
 * 8. When you use @ManyToMany, you need to use the decorator @JoinTable() to create automatically the ternary table.
 */

import {
  PrimaryGeneratedColumn,
  Column,
  Entity,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  ManyToOne,
  ManyToMany,
  JoinTable,
  Index,
  JoinColumn,
} from 'typeorm';

import { Exclude } from 'class-transformer';

import { Brand } from './brand.entity';
import { Category } from './category.entity';

@Entity({ name: 'products' })
@Index(['price', 'stock']) //Here also you can index the columns of the table
export class Product {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar', length: 255, unique: true })
  name: string;

  @Column({ type: 'text' })
  description: string;

  @Index() //With this index we can make more efficient the search of the products by price
  @Column({ type: 'int' })
  price: number;

  @Column({ type: 'int' })
  stock: number;

  @Column({ type: 'varchar' })
  image: string;

  @ManyToOne(() => Brand, (brand) => brand.products)
  @JoinColumn({ name: 'brand_id' }) //Because the product is the dominant table, and the brand is the foreign table, the name of the column must be in snake_case as an option in @JoinColumn()
  brand: Brand;

  @JoinTable({
    name: 'products_categories',
    joinColumn: { name: 'product_id' }, //the firt column name is the name of the table where you are
    inverseJoinColumn: { name: 'category_id' },
  })
  @ManyToMany(() => Category, (category) => category.products)
  categories: Category[];

  @Exclude()
  @CreateDateColumn({
    name: 'create_at',
    type: 'timestamptz',
    default: () => 'CURRENT_TIMESTAMP',
  })
  createdAt: Date;

  @Exclude()
  @UpdateDateColumn({
    name: 'update_at',
    type: 'timestamptz',
    default: () => 'CURRENT_TIMESTAMP',
  })
  updatedAt: Date;

  @Exclude()
  @DeleteDateColumn({ name: 'detele_at', type: 'timestamptz' })
  deletedAt: Date;
}
