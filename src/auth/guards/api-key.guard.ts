/**
 * This is a guard, which is used to protect the API key route. you can crate with the command " nest g gu <folder>"
 */

import {
  CanActivate,
  ExecutionContext,
  Injectable,
  UnauthorizedException,
  Inject,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Observable } from 'rxjs';
import { IS_PUBLIC_KEY } from './../decorators/public.decorator';
import { ConfigType } from '@nestjs/config';
import config from 'src/config';

import { Request } from 'express';

@Injectable()
export class ApiKeyGuard implements CanActivate {
  /**
   *
   */
  constructor(
    private reflector: Reflector,
    @Inject(config.KEY) private configService: ConfigType<typeof config>,
  ) {}

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const isPublic = this.reflector.get(IS_PUBLIC_KEY, context.getHandler()); //Get the metadata from the handler
    if (isPublic) {
      return true;
    }
    const request = context.switchToHttp().getRequest<Request>();
    const authHeader = request.header('Authorization');
    const isAuth = authHeader === this.configService.apiKey;
    if (!isAuth) {
      throw new UnauthorizedException('Invalid API key');
    }
    return isAuth;
  }
}
