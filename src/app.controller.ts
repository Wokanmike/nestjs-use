/** This is a controller, this is used to handle the requests, you have to pay attention on:
 * 1. The controller must be a class
 * 2. The controller must be exportable
 * 3. The controller must have the @Controller decorator
 * 4. The http methods are used to handle the requests, you can use the @Get, @Post, @Put, @Delete decorators
 * 5. You need to import the services on the constructor.
 */

import { Controller, Get, UseGuards, SetMetadata } from '@nestjs/common';
import { AppService } from './app.service';

import { Public } from './auth/decorators/public.decorator';
import { ApiKeyGuard } from './auth/guards/api-key.guard'; //Import the guard

@UseGuards(ApiKeyGuard) //Protect the endpoint with the guard
@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Public() //Make the endpoint public, could use @SetMetadata('isPublic', true) and setting on the guard const isPublic = this.reflector.get('isPublic', context.getHandler())
  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @Get('nuevo')
  @Public() //Make the endpoint public but builded with a custom decorator
  newEndpoint() {
    return 'This is a new endpoint';
  }

  @Get('tasks')
  tasks() {
    return this.appService.getTasks();
  }
}
