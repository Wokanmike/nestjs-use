/** This is a service, you need to have the next considers:
 * 1. The service must be a class
 * 2. The service must be exportable
 * 3. The service must be a singleton (you can use the @Injectable decorator)
 * 4. As you can see on the constructor, you can have dependency injections, this dependencies are created by the @Inject decorator.
 *   - Here are several examples oh how the injections can be used
 * 5. The methods at the end would be used by the constructor, to create the instance of the service.
 */

import { Injectable, Inject } from '@nestjs/common';
import { ConfigType } from '@nestjs/config';
import { Client } from 'pg';
import config from './config';

@Injectable()
export class AppService {
  constructor(
    // @Inject('API_KEY') private readonly apiKey: string,
    @Inject('TASKS') private tasks: any,
    @Inject(config.KEY) private configService: ConfigType<typeof config>,
    @Inject('PG') private clientPg: Client,
  ) {}
  getHello(): string {
    console.log(this.tasks);
    console.log(this.configService.apiKey);
    console.log(this.configService.database.name);
    return `The Key ${this.configService.apiKey}`;
  }
  getTasks() {
    return 'hihi';
  }
}
